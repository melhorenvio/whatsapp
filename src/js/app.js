console.time('Initialize'); // eslint-disable-line no-console
import App from './app/index';
import Contacts from './components/contacts';

const app = new App();

app.init(() => {
	const contacts = new Contacts();

	contacts.init();

	console.timeEnd('Initialize'); // eslint-disable-line no-console
});

export default class {
	constructor($element) {
		this.elementSelector = '.form__combobox';
		this.optionSelector = `${this.elementSelector}__option`;
		this.placeholderSelector = `${this.elementSelector}__placeholder`;
		this.elementOpenClass = `${this.elementSelector}--open`.substr(1);
		this.open = false;

		this.$element = $element;
		this.$options = this.$element.querySelectorAll(this.optionSelector);
		this.$placeholder = this.$element.querySelector(this.placeholderSelector);
	}

	elementToggle(state = !this.open) {
		const action = state ? 'add' : 'remove';

		this.open = state;
		this.$element.classList[action](this.elementOpenClass);
	}

	elementOpen() {
		this.elementToggle(true);
	}

	elementClose() {
		this.elementToggle(false);
	}

	selectOption(value) {
		this.$placeholder.innerText = value;
	}

	get handlers() {
		return {
			$element: {
				click: () => {
					this.elementToggle();
				},

				keydown: (e) => {
					if (e.keyCode === 13) {
						this.elementOpen();
					}

					if (e.keyCode === 27) {
						this.elementClose();
					}
				}
			},

			$options: {
				click: (e) => {
					this.selectOption(e.target.innerText);
				}
			},

			window: {
				click: (e) => {
					if (this.$element.contains(e.target) || !this.open) {
						return;
					}

					this.elementClose();
				}
			}
		};
	}

	bind() {
		this.$element.addEventListener('click', this.handlers.$element.click, false);
		this.$element.addEventListener('keydown', this.handlers.$element.keydown, false);
		this.$options.forEach(($option) => {
			$option.addEventListener('click', this.handlers.$options.click, false);
		});
		window.addEventListener('click', this.handlers.window.click, false);
	}

	init() {
		this.bind();
	}
}

export default class {
	constructor() {
		this.elementSelector = '.navbar';
		this.menuSelector = `${this.elementSelector}__menu`;
		this.elementOpenClass = `${this.elementSelector}--open`.substr(1);
		this.open = false;

		this.$element = document.querySelector(this.elementSelector);
		this.$menu = this.$element.querySelector(this.menuSelector);
	}

	toggle(state = !this.open) {
		const action = state ? 'add' : 'remove';

		this.open = state;
		this.$element.classList[action](this.elementOpenClass);
	}

	open() {
		this.toggle(true);
	}

	close() {
		this.toggle(false);
	}

	get handlers() {
		return {
			$menu: {
				click: () => {
					this.toggle();
				}
			},

			window: {
				click: (e) => {
					if (this.$element.contains(e.target) || !this.open) {
						return;
					}

					this.close();
				}
			}
		};
	}

	bind() {
		this.$menu.addEventListener('click', this.handlers.$menu.click, false);
		window.addEventListener('click', this.handlers.window.click, false);
	}

	init() {
		this.bind();
	}
}

export default class {
	constructor() {
		this.$contactsInput = document.querySelector('#contacts-input');
		this.$messageInput = document.querySelector('#message-input');
		this.$contactsContainer = document.querySelector('#contacts-container');
		this.$contactsList = document.querySelector('#contacts-list');
		this.$openAll = document.querySelector('#open-all');

		this.contacts = [];
		this.message = '';
	}

	getURI(contact) {
		return `https://wa.me/${contact}?text=${encodeURI(this.message)}`;
	}

	getItem(contact) {
		const $p = document.createElement('p'),
			$a = document.createElement('a');

		$a.classList.add('link');
		$a.href = this.getURI(contact);
		$a.setAttribute('target', '_blank');
		$a.innerText = contact;

		$p.appendChild($a);

		return $p;
	}

	updateList() {
		this.$contactsList.innerHTML = '';

		if (!this.contacts.length) {
			this.$contactsList.innerHTML = 'Nenhum contato';

			return;
		}

		this.contacts.forEach((contact) => {
			this.$contactsList.appendChild(this.getItem(contact));
		});
	}

	openAll() {
		this.contacts.forEach((contact) => {
			window.open(this.getURI(contact));
		});
	}

	get handlers() {
		return {
			$contactsInput: {
				change: () => {
					let {value} = this.$contactsInput;

					this.contacts = value ? value.split('\n').filter(Number) : [];
					this.updateList();
				}
			},

			$messageInput: {
				change: () => {
					this.message = this.$messageInput.value;
					this.updateList();
				}
			},

			$openAll: {
				click: () => {
					this.openAll();
				}
			}
		};
	}

	bind() {
		this.$contactsInput.addEventListener('change', this.handlers.$contactsInput.change, false);
		this.$messageInput.addEventListener('change', this.handlers.$messageInput.change, false);
		this.$openAll.addEventListener('click', this.handlers.$openAll.click, false);
	}

	init() {
		this.bind();
	}
}
